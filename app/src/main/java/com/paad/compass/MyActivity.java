package com.paad.compass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MyActivity extends Activity {
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }
  public void setMyBearing(View V) {
	  CompassView compass = (CompassView) findViewById(R.id.compassView);
	  compass.setBearing((float) 60.0);
  }
  
}